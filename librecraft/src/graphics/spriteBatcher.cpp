#include "spriteBatcher.h"

#include <algorithm>
#include <map>
#include <string>

#include "graphicsUtil.h"
#include "shaderProgram.h"

using namespace std;

graphics::ShaderProgram
spriteBatcherCreateShaderProgram() {
  // clang-format off
  std::string vertex =
      "#version 330 core \n"
      "in vec3 positionIn; \n"
      "in vec2 texCoordIn; \n"

      "uniform mat4 projection; \n"

      "out vec2 texCoord; \n"

      "void main() \n"
      "{ \n"
      "  gl_Position = projection * vec4(positionIn, 1.0f); \n"
      "  texCoord = texCoordIn; \n"
      "} \n";

  std::string frag =
      "#version 330 core \n"
      "in vec2 texCoord; \n"

      "out vec4 color; \n"

      "uniform sampler2D texture1; \n"
      "void main() \n"
      "{ \n"
      "  color = texture(texture1, texCoord); \n"
      "} \n";
  // clang-format on

  map<string, int> attributesMap{pair<string, int>("positionIn", 0),
                                 pair<string, int>("texCoordIn", 1)};

  return graphics::ShaderProgram(vertex, frag, attributesMap);
}

namespace graphics {

SpriteBatcher::SpriteBatcher()
  : m_program{spriteBatcherCreateShaderProgram()} {
  // hard coded default value
  m_projection = glm::ortho(0.0f, 800.0f, 0.0f, 600.0f, -1.0f, 1.0f);
}

void
SpriteBatcher::addBatch(Sprite& batch) {
  m_batches.push_back(&batch);
}

void
SpriteBatcher::draw() {
  m_program.bind();
  CHECK_GL_ERROR("Binding shader program");

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  CHECK_GL_ERROR("Setting blend function");
  glEnable(GL_BLEND);
  CHECK_GL_ERROR("Enabling blending");
  glDisable(GL_DEPTH_TEST);
  CHECK_GL_ERROR("Disabling depth test");

  sort(m_batches.begin(), m_batches.end(), [](Sprite* a, Sprite* b) {
    return a->getLayer() < b->getLayer();
  });

  m_program.setUniformli("texture1", 0);
  CHECK_GL_ERROR("Setting texture uniform");

  texture::Texture* current{nullptr};

  for (Sprite* batch : m_batches) {
    glActiveTexture(GL_TEXTURE0);
    CHECK_GL_ERROR("Activating texture");
    if (&batch->getTexture() != current) {
      current = &batch->getTexture();
      current->bind();
      CHECK_GL_ERROR("Binding texture");
    }

    glm::mat4 modelViewProjection{m_projection *
                                  batch->getTransform().getMatrix()};

    m_program.setUniformMatrix4f("projection", modelViewProjection);
    CHECK_GL_ERROR("Setting projection uniform");
    batch->draw();
    CHECK_GL_ERROR("Drawing batch");
  }

  m_program.unbind();
  CHECK_GL_ERROR("Unbinding shader program");
  m_batches.clear();
}

void
SpriteBatcher::setProjection(glm::mat4 projection) {
  m_projection = projection;
}

} /* namespace graphics */
