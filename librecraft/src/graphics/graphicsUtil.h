#ifndef GLERRORUTIL_H
#define GLERRORUTIL_H

#include <GL/glew.h>
#include <spdlog/spdlog.h>
#include <string>

namespace graphics {

inline void
checkGLErrorImpl(const char* operation, const char* file, int line) {
  GLenum error;
  while ((error = glGetError()) != GL_NO_ERROR) {
    std::string errorString;
    switch (error) {
      case GL_INVALID_ENUM:
        errorString = "INVALID_ENUM";
        break;
      case GL_INVALID_VALUE:
        errorString = "INVALID_VALUE";
        break;
      case GL_INVALID_OPERATION:
        errorString = "INVALID_OPERATION";
        break;
      case GL_STACK_OVERFLOW:
        errorString = "STACK_OVERFLOW";
        break;
      case GL_STACK_UNDERFLOW:
        errorString = "STACK_UNDERFLOW";
        break;
      case GL_OUT_OF_MEMORY:
        errorString = "OUT_OF_MEMORY";
        break;
      case GL_INVALID_FRAMEBUFFER_OPERATION:
        errorString = "INVALID_FRAMEBUFFER_OPERATION";
        break;
      case GL_CONTEXT_LOST:
        errorString = "CONTEXT_LOST";
        break;
      case GL_TABLE_TOO_LARGE:
        errorString = "TABLE_TOO_LARGE";
        break;
      default:
        errorString = "UNKNOWN";
        break;
    }
    SPDLOG_ERROR("OpenGL error: {} at {}:{}: {} ({})",
                 operation,
                 file,
                 line,
                 errorString,
                 error);
  }
}

} // namespace graphics

#if NDEBUG
#define CHECK_GL_ERROR(operation) ((void)0)
#else
#define CHECK_GL_ERROR(operation)                                              \
  graphics::checkGLErrorImpl(operation, __FILE__, __LINE__)
#endif

#endif // GLERRORUTIL_H