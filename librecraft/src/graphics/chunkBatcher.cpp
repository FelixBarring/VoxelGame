#include "chunkBatcher.h"

#include <map>

#include "graphicsUtil.h"
#include "resources.h"
#include "shaderProgram.h"

using namespace std;
using namespace glm;

graphics::ShaderProgram
chunkBatcherCreateShader() {
  // TODO Refactor this the same way as the cubeBatcher...
  // That is, use constant strings for the variables.

  // clang-format off
  string vertex =
    "#version 330 core \n"

    "in vec3 positionIn; \n"
    "in vec2 lightIn; \n"
    "in vec3 normalIn; \n"
    "in vec3 texCoordIn; \n"

    "uniform mat4 modelViewProjection; \n"
    // "uniform mat4 modelView; \n"
    "uniform float sunStrenght; \n"

    "out vec3 texCoord; \n"
    "out float lightValue; \n"
    "out vec3 faceNormal; \n"

    "void main(){ \n"
    "  texCoord = vec3(texCoordIn.x, texCoordIn.y, texCoordIn.z); \n"
    "  lightValue = max(lightIn.x * sunStrenght, lightIn.y) / 16; \n"
    "  faceNormal = normalIn; \n"
    "  gl_Position =  modelViewProjection * vec4(positionIn.xyz, 1); \n"
    "} \n";

  string fragment =
    "#version 330 core \n"

    "in vec3 texCoord; \n"
    "in float lightValue; \n"
    "in vec3 faceNormal; \n"

    "uniform sampler2DArray texture1; \n"
    "uniform float sunStrenght; \n"

    // "uniform vec3 lightDirection; \n"
    "uniform vec3 diffuseLight = vec3(1.0, 1.0, 1.0); \n"
    "uniform vec3 materialDiffuse = vec3(0.5, 0.5, 0.5); \n"

    "out vec4 color; \n"

    // "vec3 calculateDiffuse() \n "
    // "{ \n "
    // "  return sunStrenght * diffuseLight * "
    // "    max(0, dot(faceNormal, normalize(lightDirection))); \n "
    // "} \n "

    "void main(){ \n"
    "  vec3 lightSum = vec3(lightValue, lightValue, lightValue); "
//    "  vec3 lightSum = (sunStrenght) * vec3(lightValue, lightValue, lightValue) + (2 - (2 * sunStrenght)) * (calculateDiffuse()); \n"
    "  color = vec4(lightSum, 1.0f) * texture(texture1, texCoord); \n"
    "} \n";

  // clang-format on

  map<string, int> attributesMap{pair<string, int>("positionIn", 0),
                                 pair<string, int>("lightIn", 1),
                                 pair<string, int>("normalIn", 2),
                                 pair<string, int>("texCoordIn", 3)};

  return graphics::ShaderProgram(vertex, fragment, attributesMap);
}

namespace graphics {

ChunkBatcher::ChunkBatcher(Camera& camera)
  : m_camera(camera)
  , m_program{chunkBatcherCreateShader()}
  , m_texture(Resources::getInstance().getTextureArray(
      config::cube_data::textures,
      config::cube_data::TEXTURE_WIDTH,
      config::cube_data::TEXTURE_HEIGHT)) {
}

int
ChunkBatcher::addBatch(int replaceId, GraphicalChunk&& batch) {
  lock_guard<mutex> lock(m_mutex);
  m_batchesToAdd.push_back(make_tuple(++m_idCounter, replaceId, move(batch)));
  return m_idCounter;
}

void
ChunkBatcher::removeBatch(int id) {
  lock_guard<mutex> lock(m_mutex);
  m_batchesToBeRemoved.push_back(id);
}

void
ChunkBatcher::draw() {
  // Done on the main thread because the thread doing opengl calls needs an
  // opengl context, which the main thread does.

  addAndRemoveBatches();

  m_program.bind();
  CHECK_GL_ERROR("Binding shader program");

  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  CHECK_GL_ERROR("Enabling GL_CULL_FACE and GL_DEPTH_TEST");

  glActiveTexture(GL_TEXTURE0);
  CHECK_GL_ERROR("Setting active texture");
  m_program.setUniformli("texture1", 0);
  CHECK_GL_ERROR("Setting texture uniform");
  m_texture.bind();
  CHECK_GL_ERROR("Binding texture");

  // m_program.setUniform3f(
  //   "lightDirection", m_sunDirection.x, m_sunDirection.y, m_sunDirection.z);
  CHECK_GL_ERROR("Setting light direction uniform");
  m_program.setUniform1f("sunStrenght", m_sunStrength);
  CHECK_GL_ERROR("Setting sun strength uniform");

  for (pair<const int, GraphicalChunk>& batch : m_batches) {
    mat4 modelView =
      m_camera.getViewMatrix() * batch.second.getTransform().getMatrix();
    mat4 modelViewProjection = m_camera.getProjectionMatrix() * modelView;

    m_program.setUniformMatrix4f("modelViewProjection", modelViewProjection);
    // m_program.setUniformMatrix4f("modelView", modelView);
    CHECK_GL_ERROR("Setting matrix uniforms");

    batch.second.drawNoneTransparent();
    CHECK_GL_ERROR("Drawing non-transparent batch");
  }

  glDisable(GL_CULL_FACE);
  CHECK_GL_ERROR("Disabling GL_CULL_FACE");

  // A second pass to draw the water/transparent stuffs
  for (pair<const int, GraphicalChunk>& batch : m_batches) {
    if (!batch.second.hasTransparent()) {
      continue;
    }

    mat4 modelView =
      m_camera.getViewMatrix() * batch.second.getTransform().getMatrix();
    mat4 modelViewProjection = m_camera.getProjectionMatrix() * modelView;
    m_program.setUniformMatrix4f("modelViewProjection", modelViewProjection);
    // m_program.setUniformMatrix4f("modelView", modelView);
    CHECK_GL_ERROR("Setting matrix uniforms for transparent batch");
    batch.second.drawTransparent();
    CHECK_GL_ERROR("Drawing transparent batch");
  }

  m_program.unbind();
  CHECK_GL_ERROR("Unbinding shader program");
}

void
ChunkBatcher::setSunStrenght(double value) {
  m_sunStrength = value;
}

void
ChunkBatcher::setSunDirection(glm::vec3 direction) {
  m_sunDirection = direction;
}

void
ChunkBatcher::addAndRemoveBatches() {
  lock_guard<mutex> lock(m_mutex);

  for (tuple<int, int, GraphicalChunk>& t : m_batchesToAdd) {
    int id{get<0>(t)};
    int replaceId{get<1>(t)};

    m_batches.emplace(id, move(get<2>(t)));
    m_batches.at(id).uploadData();

    if (replaceId != m_noRemove) {
      m_batchesToBeRemoved.push_back(replaceId);
    }
  }
  m_batchesToAdd.clear();

  // Remove all of the batches that has been requested to be removed.
  while (!m_batchesToBeRemoved.empty()) {
    vector<int>::iterator batch = m_batchesToBeRemoved.begin();
    auto batchIt = m_batches.find(*batch);

    if (batchIt != m_batches.end()) {
      m_batches.erase(batchIt);
      m_batchesToBeRemoved.erase(batch);
    }
  }
}
} // namespace graphics

// string vertex =
//    "#version 330 core \n"
//
//    "const float density = 0.01; \n"
//    "const float gradient = 6; \n"
//
//    "in vec4 positionIn; \n"
//    "in vec3 normalIn; \n"
//    "in vec3 texCoordIn; \n"
//
//    "uniform mat4 modelViewProjection; \n"
//    "uniform mat4 modelView; \n"
//    "uniform float sunStrenght; \n"
//
//    "out vec3 texCoord; \n"
//    "out float lightValue; \n"
//    "out float fogFactor; \n"
//    "out vec3 faceNormal; \n"
//
//    "void main(){ \n"
//    "  texCoord = vec3(texCoordIn.x, texCoordIn.y, texCoordIn.z); \n"
//    "  lightValue = (positionIn.w / 16) * sunStrenght; \n"
//    "  faceNormal = normalIn; \n"
//
//    "  vec4 positionView = modelView * vec4(positionIn.xyz, 1); \n"
//    "  float distance = length(positionView.xyz); \n"
//    "  fogFactor = clamp(exp(-pow((distance * density), gradient)), 0.0, 1.0);
//    \n"
//
//    "  gl_Position =  modelViewProjection * vec4(positionIn.xyz, 1); \n"
//    "} \n";
//
// string fragment =
//    "#version 330 core \n"
//
//    "in vec3 texCoord; \n"
//    "in float lightValue; \n"
//    "in float fogFactor; \n"
//
//    "in vec3 faceNormal; \n"
//
//    "uniform sampler2DArray texture1; \n"
//    "uniform float sunStrenght; \n"
//
//    "out vec4 color; \n"
//
//    "uniform vec3 lightDirection; \n"
//    "uniform vec3 fogColor; \n"
//
//    "uniform vec3 diffuseLight = vec3(0.5, 0.5, 0.5); \n"
//    "uniform vec3 materialDiffuse = vec3(0.5, 0.5, 0.5); \n"
//
//    "vec3 calculateDiffuse() \n "
//    "{ \n "
//    "  return sunStrenght * diffuseLight * materialDiffuse * max(0,
//    dot(faceNormal, normalize(lightDirection))); \n "
//    "} \n "
//
//    "void main(){ \n"
//
//    "  vec3 diffuse = calculateDiffuse() / 2; \n"
//    "  color = vec4(diffuse, 1.0f) * texture(texture1, texCoord); \n"
//
//    "  vec4 light = vec4(lightValue, lightValue, lightValue, 1) +
//    vec4(diffuse, 0); \n"
//    "  color = mix(vec4(fogColor, 1.0), light * texture(texture1, texCoord),
//    fogFactor);"
//
//    "} \n";
