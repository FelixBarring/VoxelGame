#include "shaderProgram.h"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <map>
#include <spdlog/spdlog.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include "graphicsUtil.h"

using namespace std;

namespace graphics {

ShaderProgram::ShaderProgram(const string& vertexSource,
                             const string& fragmentSource,
                             const map<string, int>& attributes) {
  // Create the shaders
  GLuint vertexID = createVertexShader(vertexSource);
  GLuint fragmentID = createFragmentShader(fragmentSource);

  programID = glCreateProgram();
  CHECK_GL_ERROR("Error creating shader program");

  if (programID == 0) {
    SPDLOG_ERROR("Error, could not create a program");
  }

  glAttachShader(programID, vertexID);
  CHECK_GL_ERROR("Error attaching vertex shader");
  glAttachShader(programID, fragmentID);
  CHECK_GL_ERROR("Error attaching fragment shader");

  // Create locations for all the attributes
  for (pair<const string, int> attribute : attributes) {

    glBindAttribLocation(programID, attribute.second, attribute.first.c_str());
    CHECK_GL_ERROR("Error binding attribute location");
    GLenum errorCheck = glGetError();
    switch (errorCheck) {
      case GL_NO_ERROR:
        break; // All is well
      case GL_INVALID_VALUE:
        SPDLOG_ERROR(
          "################################################\n\n"
          "Error, glBindAttribLocation gave a GL_INVALID_VALUE error \n\n"
          "Faulty attribute is = {}\n\n"
          "{}\n{}\n"
          "################################################",
          attribute.first,
          vertexSource,
          fragmentSource);
        break;
      case GL_INVALID_OPERATION:
        SPDLOG_ERROR(
          "################################################\n\n"
          "Error, glBindAttribLocation gave a GL_INVALID_OPERATION error.\n"
          "Faulty attribute is = {}\n\n"
          "{}\n{}\n"
          "################################################",
          attribute.first,
          vertexSource,
          fragmentSource);
        break;
    }
  }

  glLinkProgram(programID);
  CHECK_GL_ERROR("Error linking program");
  glUseProgram(programID);
  CHECK_GL_ERROR("Error using program");

  // For all uniforms, get a location and add the name and the location to the
  // map.
  GLint numberOfUniforms;
  glGetProgramiv(programID, GL_ACTIVE_UNIFORMS, &numberOfUniforms);
  CHECK_GL_ERROR("Error getting number of active uniforms");
  GLenum type;
  vector<GLchar> nameData(256);
  for (int i = 0; i < numberOfUniforms; ++i) {
    GLint arraySize = 0;
    GLsizei actualLength = 0;
    glGetActiveUniform(programID,
                       i,
                       nameData.size(),
                       &actualLength,
                       &arraySize,
                       &type,
                       &nameData[0]);
    CHECK_GL_ERROR("Error getting active uniform");
    string name((char*)&nameData[0], actualLength);

    int uniformLocation = glGetUniformLocation(programID, name.c_str());
    CHECK_GL_ERROR("Error getting uniform location");
    uniforms.insert(make_pair(name, uniformLocation));

    SPDLOG_DEBUG("Inserted new Uniform with name: {} and location: {}",
                 name,
                 uniformLocation);

    GLenum errorCheck = glGetError();
    switch (errorCheck) {
      case GL_NO_ERROR:
        break; // All is well
      case GL_INVALID_VALUE:
        SPDLOG_ERROR(
          "Error, glGetUniformLocation gave a GL_INVALID_VALUE error.");
        return;
      case GL_INVALID_OPERATION:
        SPDLOG_ERROR(
          "Error, glGetUniformLocation gave a GL_INVALID_OPERATION error.");
        return;
    }
  }

  // Program error Check
  GLint result = GL_FALSE;
  int logLength;
  glGetProgramiv(programID, GL_LINK_STATUS, &result);
  CHECK_GL_ERROR("Error getting program link status");
  glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &logLength);
  CHECK_GL_ERROR("Error getting program info log length");
  if (logLength > 0) {
    vector<char> errorMessage(logLength + 1);
    glGetProgramInfoLog(programID, logLength, NULL, &errorMessage[0]);
    CHECK_GL_ERROR("Error getting program info log");
    SPDLOG_ERROR("Program linking error: {}", &errorMessage[0]);
  }

  glDetachShader(programID, vertexID);
  CHECK_GL_ERROR("Error detaching vertex shader");
  glDetachShader(programID, fragmentID);
  CHECK_GL_ERROR("Error detaching fragment shader");

  glDeleteShader(vertexID);
  CHECK_GL_ERROR("Error deleting vertex shader");
  glDeleteShader(fragmentID);
  CHECK_GL_ERROR("Error deleting fragment shader");
}

GLuint
ShaderProgram::createVertexShader(const string& source) {
  return createShader(GL_VERTEX_SHADER, source);
}

GLuint
ShaderProgram::createFragmentShader(const string& source) {
  return createShader(GL_FRAGMENT_SHADER, source);
}

GLuint
ShaderProgram::createShader(GLenum shaderType, const string& source) {
  GLuint shaderID = glCreateShader(shaderType);
  CHECK_GL_ERROR("Error creating shader");

  char const* shaderSourcePointer = source.c_str();
  glShaderSource(shaderID, 1, &shaderSourcePointer, nullptr);
  CHECK_GL_ERROR("Error setting shader source");
  glCompileShader(shaderID);
  CHECK_GL_ERROR("Error compiling shader");

  // Error Check
  GLint result = GL_FALSE;
  int logLength;
  glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
  CHECK_GL_ERROR("Error getting shader compile status");
  glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logLength);
  CHECK_GL_ERROR("Error getting shader info log length");
  if (result == GL_FALSE) {
    vector<char> errorMessage(logLength + 1);
    glGetShaderInfoLog(shaderID, logLength, NULL, &errorMessage[0]);
    CHECK_GL_ERROR("Error getting shader info log");
    SPDLOG_ERROR("Shader compilation error:\n{}", &errorMessage[0]);
    SPDLOG_ERROR("Shader source:\n{}", source);
  }

  return shaderID;
}

void
ShaderProgram::setUniformi(string uniformName, GLuint value) {
  glUniform1ui(uniforms.find(uniformName)->second, value);
  CHECK_GL_ERROR("Error setting uniform integer");
}

void
ShaderProgram::setUniform1f(string uniformName, float value) {
  glUniform1f(uniforms.find(uniformName)->second, value);
  CHECK_GL_ERROR("Error setting uniform float");
}

void
ShaderProgram::setUniform3f(string uniformName, float x, float y, float z) {
  glUniform3f(uniforms.find(uniformName)->second, x, y, z);
  CHECK_GL_ERROR("Error setting uniform vec3");
}

void
ShaderProgram::setUniformMatrix3f(string uniformName, glm::mat3& matrix) {
  glUniformMatrix3fv(
    uniforms.find(uniformName)->second, 1, GL_FALSE, &matrix[0][0]);
  CHECK_GL_ERROR("Error setting uniform mat3");
}

void
ShaderProgram::setUniformMatrix4f(string uniformName, glm::mat4& matrix) {
  glUniformMatrix4fv(
    uniforms.find(uniformName)->second, 1, GL_FALSE, &matrix[0][0]);
  CHECK_GL_ERROR("Error setting uniform mat4");
}

void
ShaderProgram::setUniformli(string uniformName, GLuint value) {
  glUniform1i(uniforms.find(uniformName)->second, value);
  CHECK_GL_ERROR("Error setting uniform integer");
}

void
ShaderProgram::bind() {
  glUseProgram(programID);
  CHECK_GL_ERROR("Error binding shader program");
}

void
ShaderProgram::unbind() {
  glUseProgram(0);
  CHECK_GL_ERROR("Error unbinding shader program");
}

} // namespace graphics
