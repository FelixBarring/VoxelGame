#include "chunk.h"

#include <spdlog/spdlog.h>

#include <fstream>
#include <iostream>
#include <queue>

#include "graphics/chunkBatcher.h"
#include "graphics/graphicsManager.h"

#include "model/world/terrainGen/terrainGenerator.h"

using namespace std;
using namespace glm;

using namespace graphics;
using namespace config::cube_data;
using namespace config::chunk_data;
using namespace terrainGen;

namespace chunk {

Chunk::Chunk(string worldName,
             int x,
             int z,
             graphics::GraphicsManager& graphicsManager)
  : m_xLocation{x}
  , m_zLocation{z}
  , m_graphicsManager{graphicsManager}
  , m_name{createChunkName(worldName)} {
  for (int i{0}; i < CHUNK_HEIGHT / GRAPHICAL_CHUNK_HEIGHT; ++i) {
    m_graphicalChunksIds.push_back(-1);
    m_dirtyRegions.emplace(i);
  }
}

Chunk::~Chunk() {
  for (int graphicalChunk : m_graphicalChunksIds) {
    m_graphicsManager.getChunkBatcher().removeBatch(graphicalChunk);
  }
}

void
Chunk::create(CreationOptions& options) {
  generateChunk(options);

  ifstream stream;
  stream.open(m_name);
  if (!stream.fail()) {
    loadChunk();
  }
}

void
Chunk::doSunLightning() {
  for (int x{0}; x < CHUNK_WIDTH_AND_DEPTH; ++x) {
    for (int z{0}; z < CHUNK_WIDTH_AND_DEPTH; ++z) {
      vector<vec3> dummy;
      doSunLightning(dummy, x, CHUNK_HEIGHT - 1, z, true);
      m_lightsToPropagate.insert(
        m_lightsToPropagate.end(), dummy.begin(), dummy.end());
    }
  }
}

void
Chunk::collectLightFromAllNeighbors() {
  collectLightFromRightNeighbor();
  collectLightFromLeftNeighbor();
  collectLightFromBackNeighbor();
  collectLightFromFrontNeighbor();
}

void
Chunk::collectLightFromRightNeighbor() {
  collectLightFromNeighbor<0, 0, 15, 0>(m_rightNeighbor);
}

void
Chunk::collectLightFromLeftNeighbor() {
  collectLightFromNeighbor<15, 0, 0, 0>(m_leftNeighbor);
}

void
Chunk::collectLightFromBackNeighbor() {
  collectLightFromNeighbor<0, 0, 0, 15>(m_backNeighbor);
}

void
Chunk::collectLightFromFrontNeighbor() {
  collectLightFromNeighbor<0, 15, 0, 0>(m_frontNeighbor);
}

void
Chunk::propagateLights() {
  for (vec3& l : m_lightsToPropagate) {
    propagateLight(l.x, l.y, l.z, true);
  }

  m_lightsToPropagate.clear(); // Is This okay?!?

  for (vec3& l : m_otherLightSources) {
    propagateLight(l.x, l.y, l.z, false);
  }

  m_otherLightSources.clear();
}

void
Chunk::update() {
  if (m_graphicsDirty) {
    updateGraphics();
    updateNeighborGraphics();
  }
  m_graphicsDirty = false;
}

void
Chunk::forceUpdateGraphics() {
  for (int i{0}; i < CHUNK_HEIGHT / GRAPHICAL_CHUNK_HEIGHT; ++i) {
    m_dirtyRegions.emplace(i);
  }

  updateGraphics();
}

void
Chunk::updateGraphics() {
  vector<vector<vector<Voxel>>>* right = nullptr;
  vector<vector<vector<Voxel>>>* left = nullptr;
  vector<vector<vector<Voxel>>>* front = nullptr;
  vector<vector<vector<Voxel>>>* back = nullptr;

  if (m_rightNeighbor) {
    right = &(m_rightNeighbor->m_cubes);
  }

  if (m_leftNeighbor) {
    left = &(m_leftNeighbor->m_cubes);
  }

  if (m_frontNeighbor) {
    front = &(m_frontNeighbor->m_cubes);
  }

  if (m_backNeighbor) {
    back = &(m_backNeighbor->m_cubes);
  }

  for (int i : m_dirtyRegions) {

    GraphicalChunk batch(m_xLocation,
                         i * GRAPHICAL_CHUNK_HEIGHT,
                         m_zLocation,
                         m_cubes,
                         right,
                         left,
                         back,
                         front);

    int derp = m_graphicsManager.getChunkBatcher().addBatch(
      m_graphicalChunksIds[i], move(batch));

    m_graphicalChunksIds[i] = derp;
  }
  m_dirtyRegions.clear();
}

Voxel
Chunk::getVoxel(int x, int y, int z) {
  return m_cubes[x][y][z];
}

void
Chunk::setCube(int x, int y, int z, char id) {
  SPDLOG_TRACE("Setting cube at ({}, {}, {}) to id {}", x, y, z, id);
  // We can not change bedrock!
  if (m_cubes[x][y][z].getId() == BED_ROCK) {
    return;
  }

  Voxel& voxel{m_cubes[x][y][z]};
  int replacedId{voxel.getId()};
  voxel.setId(id);

  m_delta.emplace_back(ChunkModification{static_cast<uint8_t>(x),
                                         static_cast<uint8_t>(y),
                                         static_cast<uint8_t>(z),
                                         static_cast<uint8_t>(id)});
  // If we removed a cube
  if (id == AIR || id == WATER) {
    if (replacedId == LIGHT) {
      dePropagateOtherlight(x, y, z);
    }

    updateLightningCubeRemoved(voxel, x, y, z);
  } else { // We added a cube
    updateLightningCubeAdded(x, y, z);
  }
  m_deltaDirty = true;
}

void
Chunk::removeAllNeighbors() {
  if (m_leftNeighbor) {
    m_leftNeighbor->m_rightNeighbor.reset();
    m_leftNeighbor.reset();
  }
  if (m_rightNeighbor) {
    m_rightNeighbor->m_leftNeighbor.reset();
    m_rightNeighbor.reset();
  }
  if (m_frontNeighbor) {
    m_frontNeighbor->m_backNeighbor.reset();
    m_frontNeighbor.reset();
  }
  if (m_backNeighbor) {
    m_backNeighbor->m_frontNeighbor.reset();
    m_backNeighbor.reset();
  }
}

void
Chunk::storeChunk() {
  if (!m_deltaDirty) {
    return;
  }

  SPDLOG_DEBUG("Saving chunk {}", m_name);
  ofstream outStream(m_name, std::ios::binary);
  if (!outStream.is_open()) {
    SPDLOG_ERROR("Could not create a file for saving the chunk: {}", m_name);
    return;
  }

  SPDLOG_DEBUG("Writing {} modifications", m_delta.size());
  for (const ChunkModification& modification : m_delta) {
    outStream.write(reinterpret_cast<const char*>(&modification.packed_data),
                    sizeof(uint32_t));

    if (outStream.fail()) {
      SPDLOG_ERROR("Error writing to chunk file: {}", m_name);
      outStream.close();
      return;
    }
  }

  outStream.close();
  if (outStream.fail()) {
    SPDLOG_ERROR("Error closing chunk file: {}", m_name);
    return;
  }

  m_deltaDirty = false;
}

std::string
Chunk::createChunkName(std::string worldName) {
  return config::dataFolder + worldName + "_" + std::to_string(m_xLocation) +
         "_" + std::to_string(m_zLocation) + ".chunk";
}

void
Chunk::loadChunk() {
  ifstream inStream(m_name, std::ios::binary);
  if (!inStream.is_open()) {
    SPDLOG_ERROR("Could not open chunk file: {}", m_name);
    return;
  }

  // Get file size
  inStream.seekg(0, std::ios::end);
  std::streamsize size = inStream.tellg();
  inStream.seekg(0, std::ios::beg);

  // Read the entire file into a buffer
  std::vector<char> buffer(size);
  if (!inStream.read(buffer.data(), size)) {
    SPDLOG_ERROR("Error reading chunk file: {}", m_name);
    return;
  }

  // Process the buffer
  for (std::streamsize i = 0; i < size; i += sizeof(uint32_t)) {
    uint32_t packed_data;
    std::memcpy(&packed_data, &buffer[i], sizeof(uint32_t));

    ChunkModification mod{packed_data};

    uint8_t x = mod.getX();
    uint8_t y = mod.getY();
    uint8_t z = mod.getZ();
    uint8_t id = mod.getId();

    if (x >= m_width || y >= m_height || z >= m_depth) {
      SPDLOG_ERROR("Invalid coordinates in chunk file: {}", m_name);
      continue; // Skip this entry instead of throwing an exception
    }

    if (m_cubes[x][y][z].getId() != id) {
      m_cubes[x][y][z].setId(id);
      m_delta.push_back(mod);

      if (id == LIGHT) {
        m_cubes[x][y][z].setOtherLightValue(m_directSunlight);
        m_otherLightSources.push_back(vec3(x, y, z));
      } else {
        m_cubes[x][y][z].setOtherLightValue(0);
      }

      updateDirtyRegions(y);
    }
  }

  m_graphicsDirty = true;
}

void
Chunk::generateChunk(CreationOptions& options) {
  terrainGen::TerrainGenerator generator{
    config::chunk_data::CHUNK_WIDTH_AND_DEPTH,
    config::chunk_data::CHUNK_HEIGHT,
    config::chunk_data::CHUNK_WIDTH_AND_DEPTH,
    options.getSeed()};
  m_cubes = generator.generateTerrain(options, m_xLocation, m_zLocation);
}

Voxel*
Chunk::getVoxel2(int x, int y, int z) {
  if (x < m_width && x >= 0 && y < m_height && y >= 0 && z < m_depth &&
      z >= 0) {
    return &m_cubes[x][y][z];
  } else if (x == m_width &&
             (y < m_height && y >= 0 && z < m_depth && z >= 0)) {
    if (m_rightNeighbor) {
      return &(m_rightNeighbor->m_cubes[0][y][z]);
    } else {
      return nullptr;
    }
  } else if (x == -1 && (y < m_height && y >= 0 && z < m_depth && z >= 0)) {
    if (m_leftNeighbor) {
      return &(m_leftNeighbor->m_cubes[m_width - 1][y][z]);
    } else {
      return nullptr;
    }
  } else if (z == m_depth &&
             (x < m_width && x >= 0 && y < m_height && y >= 0)) {
    if (m_backNeighbor) {
      return &(m_backNeighbor->m_cubes[x][y][0]);
    } else {
      return nullptr;
    }
  } else if (z == -1 && (x < m_width && x >= 0 && y < m_height && y >= 0)) {
    if (m_frontNeighbor) {
      return &(m_frontNeighbor->m_cubes[x][y][m_depth - 1]);
    } else {
      return nullptr;
    }
  }
  return nullptr;
}

void
Chunk::updateLightningCubeRemoved(Voxel& voxel, int x, int y, int z) {
  // If the cube is adjacent to a neighbor, the neighbor needs to be update.
  if (x == m_width - 1 && m_rightNeighbor) {
    m_rightNeighbor->updateDirtyRegions(y);
  }

  if (x == 0 && m_leftNeighbor) {
    m_leftNeighbor->updateDirtyRegions(y);
  }

  if (z == m_depth - 1 && m_backNeighbor) {
    m_backNeighbor->updateDirtyRegions(y);
  }

  if (z == 0 && m_frontNeighbor) {
    m_frontNeighbor->updateDirtyRegions(y);
  }

  if (isInDirectSunlight(x, y, z)) {
    vector<vec3> lightPropagate;
    doSunLightning(lightPropagate, x, y, z);
    for (vec3& v : lightPropagate) {
      propagateLight(v.x, v.y, v.z, true);
    }

  } else {
    int highestSLV{std::max(highestSunLVFromNeighbors(x, y, z) - 1, 0)};
    voxel.setSunLightValue(highestSLV);
    propagateLight(x, y, z, true);
  }

  int highestOLV{std::max(highestOtherLVFromNeighbors(x, y, z) - 1, 0)};
  voxel.setOtherLightValue(highestOLV);
  propagateLight(x, y, z, false);

  // TODO This should not be done here...
  updateDirtyRegions(y);

  m_graphicsDirty = true;
}

void
Chunk::updateLightningCubeAdded(int x, int y, int z) {
  if (isInDirectSunlight(x, y, z)) {
    for (int i = y; i >= 0; --i) {
      m_cubes[x][i][z].setSunLightValue(0);
    }

    for (int i = y; i >= 0; --i) {
      dePropagateSunlight(x, i, z, 15);
    }
  }
  dePropagateSunlight(x, y, z);

  Voxel& v{m_cubes[x][y][z]};
  if (v.getId() == LIGHT) {
    v.setOtherLightValue(m_directSunlight);
    propagateLight(x, y, z, false);
  } else {
    dePropagateOtherlight(x, y, z);
  }

  updateDirtyRegions(y);

  m_graphicsDirty = true;
}

void
Chunk::updateNeighborGraphics() {
  if (m_rightNeighbor) {
    m_rightNeighbor->updateGraphics();

    if (m_rightNeighbor->m_backNeighbor) {
      m_rightNeighbor->m_backNeighbor->updateGraphics();
    }

    if (m_rightNeighbor->m_frontNeighbor) {
      m_rightNeighbor->m_frontNeighbor->updateGraphics();
    }
  }

  if (m_leftNeighbor) {
    m_leftNeighbor->updateGraphics();

    if (m_leftNeighbor->m_backNeighbor) {
      m_leftNeighbor->m_backNeighbor->updateGraphics();
    }

    if (m_leftNeighbor->m_frontNeighbor) {
      m_leftNeighbor->m_frontNeighbor->updateGraphics();
    }
  }

  if (m_backNeighbor) {
    m_backNeighbor->updateGraphics();
  }

  if (m_frontNeighbor) {
    m_frontNeighbor->updateGraphics();
  }
}

void
Chunk::doSunLightning(vector<vec3>& lightPropagate,
                      int x,
                      int y,
                      int z,
                      bool useVec) {

  static int lightReduction{1};
  // Sun lightning, only air and water gets light.
  // Each step in water reduces the light strength lightReduction.
  int lightValue{m_directSunlight};
  for (int i{y}; i >= 0; --i) {
    Voxel& cube = m_cubes[x][i][z];

    if (cube.getId() == AIR || cube.getId() == WATER) {
      if (cube.getId() == WATER && lightValue > 0) {
        lightValue -= lightReduction;
      }

      cube.setSunLightValue(lightValue);
      if (useVec) {
        lightPropagate.push_back(vec3(x, i, z));
      }

    } else {
      return;
    }
  }
}

void
Chunk::propagateLight(int x, int y, int z, bool isSunLight) {
  Voxel& voxel = m_cubes[x][y][z];
  int lvInitial =
    isSunLight ? voxel.getSunLightValue() - 1 : voxel.getOtherLightValue() - 1;
  vector<vec3> newPropagates;

  // Helper lambda to update light value and add to propagation list
  auto updateLight = [&](Voxel& v, int lv, int nx, int ny, int nz) {
    if (v.getId() == AIR &&
        (isSunLight ? v.getSunLightValue() : v.getOtherLightValue()) < lv) {
      if (isSunLight) {
        v.setSunLightValue(lv);
      } else {
        v.setOtherLightValue(lv);
      }
      newPropagates.push_back(vec3(nx, ny, nz));
      updateDirtyRegions(ny);
      return true;
    }
    return false;
  };

  // Propagate in all 6 directions
  for (const auto& dir : {
         vec3(1, 0, 0),
         vec3(-1, 0, 0), // right, left
         vec3(0, 1, 0),
         vec3(0, -1, 0), // up, down
         vec3(0, 0, 1),
         vec3(0, 0, -1) // back, front
       }) {
    int lv = lvInitial;
    for (int i = 1; lv > 0; ++i) {
      int nx = x + i * dir.x;
      int ny = y + i * dir.y;
      int nz = z + i * dir.z;

      if (nx >= 0 && nx < m_width && ny >= 0 && ny < m_height && nz >= 0 &&
          nz < m_depth) {
        if (!updateLight(m_cubes[nx][ny][nz], lv, nx, ny, nz)) {
          break;
        }
        --lv;
      } else {
        // Handle neighbor chunks

        auto getNeighborChunk = [this](int x, int z) -> Chunk* {
          if (x < 0) {
            return m_leftNeighbor.get();
          }
          if (x >= m_width) {
            return m_rightNeighbor.get();
          }
          if (z < 0) {
            return m_frontNeighbor.get();
          }
          if (z >= m_depth) {
            return m_backNeighbor.get();
          }
          return nullptr;
        };

        auto neighbor = getNeighborChunk(nx, nz);
        if (neighbor) {
          int nnx = (nx + m_width) % m_width;
          int nnz = (nz + m_depth) % m_depth;
          if (updateLight(neighbor->m_cubes[nnx][ny][nnz], lv, nnx, ny, nnz)) {
            neighbor->propagateLight(nnx, ny, nnz, isSunLight);
          }
        }
        break;
      }
    }
  }

  // Propagate to new points
  for (const auto& point : newPropagates) {
    propagateLight(point.x, point.y, point.z, isSunLight);
  }
}

void
Chunk::updateDirtyRegions(int y) {
  int region = y / GRAPHICAL_CHUNK_HEIGHT;
  m_dirtyRegions.emplace(region);
}

void
Chunk::dePropagateSunlight(int x, int y, int z, int _lightValue) {
  Voxel& voxel = m_cubes[x][y][z];

  int lightValue = voxel.getSunLightValue();
  if (_lightValue != -1) {
    lightValue = _lightValue;
  }

  voxel.setSunLightValue(0);
  updateDirtyRegions(y);

  // ########################################################################

  // Right
  if (x < m_width - 1) {
    if (m_cubes[x + 1][y][z].getId() == AIR &&
        m_cubes[x + 1][y][z].getSunLightValue() < lightValue &&
        highestSunLVFromNeighbors(x + 1, y, z) <= lightValue) {
      dePropagateSunlight(x + 1, y, z);
    } else {
      propagateLight(x + 1, y, z, true);
    }

  } else if (x == m_width - 1) {
    if (m_rightNeighbor && m_rightNeighbor->m_cubes[0][y][z].getId() == AIR &&
        m_rightNeighbor->m_cubes[0][y][z].getSunLightValue() < lightValue &&
        m_rightNeighbor->highestSunLVFromNeighbors(0, y, z) <= lightValue) {
      m_rightNeighbor->dePropagateSunlight(0, y, z);
    } else {
      m_rightNeighbor->propagateLight(0, y, z, true);
    }
  }

  // Left
  if (x > 0) {
    if (m_cubes[x - 1][y][z].getId() == AIR &&
        m_cubes[x - 1][y][z].getSunLightValue() < lightValue &&
        highestSunLVFromNeighbors(x - 1, y, z) <= lightValue) {
      dePropagateSunlight(x - 1, y, z);
    } else {
      propagateLight(x - 1, y, z, true);
    }

  } else if (x == 0) {
    if (m_leftNeighbor &&
        m_leftNeighbor->m_cubes[m_width - 1][y][z].getId() == AIR &&
        m_leftNeighbor->m_cubes[m_width - 1][y][z].getSunLightValue() <
          lightValue &&
        m_leftNeighbor->highestSunLVFromNeighbors(m_width - 1, y, z) <=
          lightValue) {
      m_leftNeighbor->dePropagateSunlight(m_width - 1, y, z);
    } else {
      m_leftNeighbor->propagateLight(m_width - 1, y, z, true);
    }
  }

  // ########################################################################

  // Up
  if (y < m_height - 1) {
    if (m_cubes[x][y + 1][z].getId() == AIR &&
        m_cubes[x][y + 1][z].getSunLightValue() < lightValue &&
        highestSunLVFromNeighbors(x, y + 1, z) <= lightValue) {
      dePropagateSunlight(x, y + 1, z);
    } else {
      propagateLight(x, y + 1, z, true);
    }
  }

  // Down
  if (_lightValue == -1 && y > 0) {
    if (m_cubes[x][y - 1][z].getId() == AIR &&
        m_cubes[x][y - 1][z].getSunLightValue() < lightValue &&
        highestSunLVFromNeighbors(x, y - 1, z) <= lightValue) {
      dePropagateSunlight(x, y - 1, z);
    } else {
      propagateLight(x, y - 1, z, true);
    }
  }

  // ########################################################################

  // Backwards
  if (z < m_depth - 1) {
    if (m_cubes[x][y][z + 1].getId() == AIR &&
        m_cubes[x][y][z + 1].getSunLightValue() < lightValue &&
        highestSunLVFromNeighbors(x, y, z + 1) <= lightValue) {
      dePropagateSunlight(x, y, z + 1);
    } else {
      propagateLight(x, y, z + 1, true);
    }

  } else if (z == m_depth - 1) {

    if (m_backNeighbor && m_backNeighbor->m_cubes[x][y][0].getId() == AIR &&
        m_backNeighbor->m_cubes[x][y][0].getSunLightValue() < lightValue &&
        m_backNeighbor->highestSunLVFromNeighbors(x, y, 0) <= lightValue) {
      m_backNeighbor->dePropagateSunlight(x, y, 0);
    } else {
      m_rightNeighbor->propagateLight(x, y, 0, true);
    }
  }

  // Forwards
  if (z > 0) {
    if (m_cubes[x][y][z - 1].getId() == AIR &&
        m_cubes[x][y][z - 1].getSunLightValue() < lightValue &&
        highestSunLVFromNeighbors(x, y, z - 1) <= lightValue) {
      dePropagateSunlight(x, y, z - 1);
    } else {
      propagateLight(x, y, z - 1, true);
    }

  } else if (z == 0) {
    if (m_frontNeighbor &&
        m_frontNeighbor->m_cubes[x][y][m_depth - 1].getId() == AIR &&
        m_frontNeighbor->m_cubes[x][y][m_depth - 1].getSunLightValue() <
          lightValue &&
        m_frontNeighbor->highestSunLVFromNeighbors(x, y, m_depth - 1) <=
          lightValue) {
      m_frontNeighbor->dePropagateSunlight(x, y, m_depth - 1);
    } else {
      m_frontNeighbor->propagateLight(x, y, m_depth - 1, true);
    }
  }
}

void
Chunk::dePropagateOtherlight(int x, int y, int z) {
  updateDirtyRegions(y);

  queue<vec3> depropagates;
  depropagates.emplace(x, y, z);
  vector<vec3> propagates;

  while (!depropagates.empty()) {

    vec3 current = depropagates.front();
    depropagates.pop();
    Voxel& voxel = m_cubes[current.x][current.y][current.z];
    char lightValue{voxel.getOtherLightValue()};
    voxel.setOtherLightValue(0);

    // Right
    if (current.x + 1 < m_width) {
      Voxel& v{m_cubes[current.x + 1][current.y][current.z]};
      if (v.getId() == AIR) {
        if (v.getOtherLightValue() != 0 &&
            v.getOtherLightValue() <= lightValue) {
          depropagates.emplace(current.x + 1, current.y, current.z);
        } else if (v.getOtherLightValue() >= lightValue) {
          propagates.push_back({current.x + 1, current.y, current.z});
        }
      }
    } else {
      if (m_rightNeighbor) {
        Voxel& v{m_rightNeighbor->m_cubes[0][current.y][current.z]};
        if (v.getId() == AIR) {
          if (v.getOtherLightValue() != 0 &&
              v.getOtherLightValue() <= lightValue) {
            m_rightNeighbor->dePropagateOtherlight(0, current.y, current.z);
          }
        }
      }
    }

    // Left
    if (current.x - 1 >= 0) {
      Voxel& v{m_cubes[current.x - 1][current.y][current.z]};
      if (v.getId() == AIR) {
        if (v.getOtherLightValue() != 0 &&
            v.getOtherLightValue() <= lightValue) {
          depropagates.emplace(current.x - 1, current.y, current.z);
        } else if (v.getOtherLightValue() >= lightValue) {
          propagates.push_back({current.x - 1, current.y, current.z});
        }
      }
    } else {
      if (m_leftNeighbor) {
        Voxel& v{m_leftNeighbor->m_cubes[m_width - 1][current.y][current.z]};
        if (v.getId() == AIR) {
          if (v.getOtherLightValue() != 0 &&
              v.getOtherLightValue() <= lightValue) {
            m_leftNeighbor->dePropagateOtherlight(
              m_width - 1, current.y, current.z);
          }
        }
      }
    }

    // Up
    if (current.y + 1 < m_height) {
      Voxel& v{m_cubes[current.x][current.y + 1][current.z]};
      if (v.getId() == AIR) {
        if (v.getOtherLightValue() != 0 &&
            v.getOtherLightValue() <= lightValue) {
          depropagates.emplace(current.x, current.y + 1, current.z);
        } else if (v.getOtherLightValue() >= lightValue) {
          propagates.push_back({current.x, current.y + 1, current.z});
        }
      }
    }

    // Down
    if (current.y - 1 >= 0) {
      Voxel& v{m_cubes[current.x][current.y - 1][current.z]};
      if (v.getId() == AIR) {
        if (v.getOtherLightValue() != 0 &&
            v.getOtherLightValue() <= lightValue) {
          depropagates.emplace(current.x, current.y - 1, current.z);
        } else if (v.getOtherLightValue() >= lightValue) {
          propagates.push_back({current.x, current.y - 1, current.z});
        }
      }
    }

    // Backwards
    if (current.z + 1 > 0) {
      Voxel& v{m_cubes[current.x][current.y][current.z + 1]};
      if (v.getId() == AIR) {
        if (v.getOtherLightValue() != 0 &&
            v.getOtherLightValue() <= lightValue) {
          depropagates.emplace(current.x, current.y, current.z + 1);
        } else if (v.getOtherLightValue() >= lightValue) {
          propagates.push_back({current.x, current.y, current.z + 1});
        }
      }
    } else {
      if (m_backNeighbor) {
        Voxel& v{m_cubes[current.x][current.y][0]};
        if (v.getId() == AIR) {
          if (v.getOtherLightValue() != 0 &&
              v.getOtherLightValue() <= lightValue) {
            m_backNeighbor->dePropagateOtherlight(current.x, current.y, 0);
          }
        }
      }
    }

    // Forward
    if (current.z - 1 >= 0) {
      Voxel& v{m_cubes[current.x][current.y][current.z - 1]};
      if (v.getId() == AIR) {
        if (v.getOtherLightValue() != 0 &&
            v.getOtherLightValue() <= lightValue) {
          depropagates.emplace(current.x, current.y, current.z - 1);
        } else if (v.getOtherLightValue() >= lightValue) {
          propagates.push_back({current.x, current.y, current.z - 1});
        }
      }
    } else {
      if (m_frontNeighbor) {
        Voxel& v{m_cubes[current.x][current.y][m_depth - 1]};
        if (v.getId() == AIR) {
          if (v.getOtherLightValue() != 0 &&
              v.getOtherLightValue() <= lightValue) {
            m_frontNeighbor->dePropagateOtherlight(
              current.x, current.y, m_depth - 1);
          }
        }
      }
    }
  }

  for (vec3& p : propagates) {
    propagateLight(p.x, p.y, p.z, false);
  }
}

int
Chunk::highestSunLVFromNeighbors(int x, int y, int z) {
  int highestValue = -1;

  Voxel* v = getVoxel2(x + 1, y, z);
  if (v && v->getSunLightValue() > highestValue) {
    highestValue = v->getSunLightValue();
  }

  v = getVoxel2(x - 1, y, z);
  if (v && v->getSunLightValue() > highestValue) {
    highestValue = v->getSunLightValue();
  }

  v = getVoxel2(x, y + 1, z);
  if (v && v->getSunLightValue() > highestValue) {
    highestValue = v->getSunLightValue();
  }

  v = getVoxel2(x, y - 1, z);
  if (v && v->getSunLightValue() > highestValue) {
    highestValue = v->getSunLightValue();
  }

  v = getVoxel2(x, y, z + 1);
  if (v && v->getSunLightValue() > highestValue) {
    highestValue = v->getSunLightValue();
  }

  v = getVoxel2(x, y, z - 1);
  if (v && v->getSunLightValue() > highestValue) {
    highestValue = v->getSunLightValue();
  }

  return highestValue;
}

int
Chunk::highestOtherLVFromNeighbors(int x, int y, int z) {
  int highestValue = -1;

  Voxel* v = getVoxel2(x + 1, y, z);
  if (v && v->getOtherLightValue() > highestValue) {
    highestValue = v->getOtherLightValue();
  }

  v = getVoxel2(x - 1, y, z);
  if (v && v->getOtherLightValue() > highestValue) {
    highestValue = v->getOtherLightValue();
  }

  v = getVoxel2(x, y + 1, z);
  if (v && v->getOtherLightValue() > highestValue) {
    highestValue = v->getOtherLightValue();
  }

  v = getVoxel2(x, y - 1, z);
  if (v && v->getOtherLightValue() > highestValue) {
    highestValue = v->getOtherLightValue();
  }

  v = getVoxel2(x, y, z + 1);
  if (v && v->getOtherLightValue() > highestValue) {
    highestValue = v->getOtherLightValue();
  }

  v = getVoxel2(x, y, z - 1);
  if (v && v->getOtherLightValue() > highestValue) {
    highestValue = v->getOtherLightValue();
  }

  return highestValue;
}

bool
Chunk::isInDirectSunlight(int x, int y, int z) {
  if (y < m_height) {
    return getVoxel(x, y + 1, z).getSunLightValue() == m_directSunlight;
  }

  return false;
}
} // namespace chunk
