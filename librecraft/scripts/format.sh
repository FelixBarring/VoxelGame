#!/bin/sh
# Will return an exit code 0 if there is no git diff, and 1 if ther is.
find ../src -name "*.h" -o -name "*.cpp" | grep -v './dependencies' | xargs clang-format -i -style=file
git diff > dummyFile
SIZE='wc -c "$filename"'
RET=0
if [ "$SIZE" != 0 ]; then
   RET=1
fi
rm dummyFile
exit "$RET"
