cmake_minimum_required(VERSION 3.21)
project(VoxelGame)

# Options
option(UNIT_TEST "Build unit tests" OFF)
option(APPLICATION "Build the main application" ON)
option(DEMO "Build the demo" OFF)

# Compiler settings
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Warning flags
set(WARNING_FLAGS
    -pedantic -Wall -Wextra
    # Add other warning flags as needed
    # -Wcast-align
    # -Wcast-qual
    # -Wctor-dtor-privacy
    # -Wdisabled-optimization
    # -Wformat=2
    # -Winit-self
    # -Wlogical-op
    # -Wmissing-declarations
    # -Wmissing-include-dirs
    # -Wnoexcept
    # -Wold-style-cast
    # -Woverloaded-virtual
    # -Wredundant-decls
    # -Wshadow
    # -Wsign-conversion
    # -Wsign-promo
    # -Wstrict-null-sentinel
    # -Wstrict-overflow=5
    # -Wswitch-default
    # -Wundef
    # -Werror
    # -Wno-unused
    # -Weffc++
)

option(ENABLE_ALL_LOGS "Enable all log levels including debug and trace" ON)

if(ENABLE_ALL_LOGS)
    add_compile_definitions(SPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_DEBUG)
    message("### All log levels enabled, including debug and trace")
else()
    if(CMAKE_BUILD_TYPE MATCHES Debug)
        add_compile_definitions(SPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_DEBUG)
        message("### Debug build. Debug logs enabled")
    else()
        add_compile_definitions(SPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_INFO)
        message("### Release build. Debug logs disabled")
    endif()
endif()

# Debug/Release specific settings
if(CMAKE_BUILD_TYPE MATCHES Debug)
    message("### Debug build.")
    add_compile_options(-O0)
elseif(CMAKE_BUILD_TYPE MATCHES Release)
    message("### Release build. Using -O3 optimization")
    add_compile_options(-O3)
else()
    message("### No build type specified.")
endif()

# Include directories
set(INCLUDE_DIRS 
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_SOURCE_DIR}/dependencies/include
    ${CMAKE_SOURCE_DIR}/dependencies/include/ThreadPool
)

# Source files
file(GLOB_RECURSE SRC_LIST "${CMAKE_SOURCE_DIR}/src/*.cpp")
# file(GLOB_RECURSE COMMON_SRC "${CMAKE_SOURCE_DIR}/src/entities/*.cpp"
#                              "${CMAKE_SOURCE_DIR}/src/graphics/*.cpp"
#                              "${CMAKE_SOURCE_DIR}/src/util/*.cpp"
#                              "${CMAKE_SOURCE_DIR}/src/config/*.cpp"
#                              "${CMAKE_SOURCE_DIR}/src/model/*.cpp"
#                              "${CMAKE_SOURCE_DIR}/src/gui/*.cpp")
# Could be solved by restructuring so that demo is not ins src, or there is no files in root src
list(FILTER SRC_LIST EXCLUDE REGEX "${CMAKE_SOURCE_DIR}/src/demo/.*\\.cpp$")

if(DEMO)
    file(GLOB_RECURSE DEMO_SRC "${CMAKE_SOURCE_DIR}/src/demo/*.cpp")
endif()

# Dependencies
add_subdirectory(dependencies/glew-cmake/)
add_subdirectory(dependencies/dependencies_2)

# SFML
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
find_package(SFML 2 REQUIRED system window graphics audio)

# Main application
if(APPLICATION)
    add_executable(${PROJECT_NAME} ${SRC_LIST})
    target_include_directories(${PROJECT_NAME} PRIVATE ${INCLUDE_DIRS} ${SFML_INCLUDE_DIR})
    target_compile_options(${PROJECT_NAME} PRIVATE ${WARNING_FLAGS})
    target_link_libraries(${PROJECT_NAME} PRIVATE libnoise tinyXml2 pthread libglew_static ${SFML_LIBRARIES})
    
    if(NOT UNIT_TEST)
        target_compile_definitions(${PROJECT_NAME} PRIVATE DOCTEST_CONFIG_DISABLE)
    endif()
endif()

# Demo
if(DEMO)
    add_executable(Demos ${DEMO_SRC} ${COMMON_SRC})
    target_include_directories(Demos PRIVATE 
        ${INCLUDE_DIRS}
        ${CMAKE_SOURCE_DIR}/dependencies/dependencies_2/libnoise/include
        ${SFML_INCLUDE_DIR}
    )
    target_link_libraries(Demos PRIVATE libnoise tinyXml2 pthread libglew_static ${SFML_LIBRARIES})

    if(NOT UNIT_TEST)
        target_compile_definitions(Demos PRIVATE DOCTEST_CONFIG_DISABLE)
    endif()
endif()

# Create compile_commands.json symlink
if(CMAKE_EXPORT_COMPILE_COMMANDS)
    add_custom_target(
        create_symlink ALL
        COMMAND ${CMAKE_COMMAND} -E create_symlink
                ${CMAKE_BINARY_DIR}/compile_commands.json
                ${CMAKE_SOURCE_DIR}/compile_commands.json
    )
endif()

# Output messages
message("### Building with ${CMAKE_CXX_COMPILER_ID}")
message("### SFML Include dir = ${SFML_INCLUDE_DIR}")
message("### SFML Libraries = ${SFML_LIBRARIES}")